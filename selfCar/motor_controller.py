import RPi.GPIO as GPIO


class CCUPiMotor:
    def __init__(self, motorR1_pin, motorR2_pin, motorL1_pin, motorL2_pin):
        self.motorR1_pin = motorR1_pin
        self.motorR2_pin = motorR2_pin
        self.motorL1_pin = motorL1_pin
        self.motorL2_pin = motorL2_pin
        
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.motorR1_pin, GPIO.OUT)
        GPIO.setup(self.motorR2_pin, GPIO.OUT)
        GPIO.setup(self.motorL1_pin, GPIO.OUT)
        GPIO.setup(self.motorL2_pin, GPIO.OUT)
        GPIO.setup(15, GPIO.OUT)
        GPIO.output(15, GPIO.HIGH)

        self.motorR1 = GPIO.PWM(self.motorR1_pin, 500)
        self.motorR2 = GPIO.PWM(self.motorR2_pin, 500)
        self.motorL1 = GPIO.PWM(self.motorL1_pin, 500)
        self.motorL2 = GPIO.PWM(self.motorL2_pin, 500)

        self.motorR1.start(0)
        self.motorR2.start(0)
        self.motorL1.start(0)
        self.motorL2.start(0)
        
    def stop(self):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(0)
        self.motorL1.ChangeDutyCycle(0)
        self.motorL2.ChangeDutyCycle(0)
    
    def forward(self, speed):
        self.motorR1.ChangeDutyCycle(speed)
        self.motorR2.ChangeDutyCycle(0)
        self.motorL1.ChangeDutyCycle(speed)
        self.motorL2.ChangeDutyCycle(0)
        
    def backword(self, speed):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(speed)
        self.motorL1.ChangeDutyCycle(0)
        self.motorL2.ChangeDutyCycle(speed)
        
    def rotateClockwise(self, speed):
        self.motorR1.ChangeDutyCycle(0)
        self.motorR2.ChangeDutyCycle(speed)
        self.motorL1.ChangeDutyCycle(speed)
        self.motorL2.ChangeDutyCycle(0)
    
    def rotateCounterclockwise(self, speed):
        self.motorR1.ChangeDutyCycle(speed)
        self.motorR2.ChangeDutyCycle(0)
        self.motorL1.ChangeDutyCycle(0)
        self.motorL2.ChangeDutyCycle(speed)

    def setSpeed(self, lSpeed, rSpeed):
        if lSpeed < 0:
            self.motorL1.ChangeDutyCycle(0)
            self.motorL2.ChangeDutyCycle(-lSpeed)
        elif lSpeed >= 0:
            self.motorL1.ChangeDutyCycle(lSpeed)
            self.motorL2.ChangeDutyCycle(0)

        if rSpeed < 0:
            self.motorR1.ChangeDutyCycle(0)
            self.motorR2.ChangeDutyCycle(-rSpeed)
        elif rSpeed >= 0:
            self.motorR1.ChangeDutyCycle(rSpeed)
            self.motorR2.ChangeDutyCycle(0)

    def __delete__(self):
        self.motorR1.stop()
        self.motorR2.stop()
        self.motorL1.stop()
        self.motorL2.stop()
        GPIO.cleanup()


class CCUPiMotor_4Motor:
    def __init__(self, frontR1_pin, frontR2_pin, frontL1_pin, frontL2_pin, backR1_pin, backR2_pin, backL1_pin,
                 backL2_pin):
        self.frontR1_pin = frontR1_pin
        self.frontR2_pin = frontR2_pin
        self.frontL1_pin = frontL1_pin
        self.frontL2_pin = frontL2_pin
        self.backR1_pin = backR1_pin
        self.backR2_pin = backR2_pin
        self.backL1_pin = backL1_pin
        self.backL2_pin = backL2_pin
        GPIO.cleanup()
        print(
            "Init motor: frontR1_pin:{0}, frontR2_pin:{1}, frontL1_pin:{2}, frontL2_pin:{3}, backR1_pin:{4}, backR2_pin:{5}, backL1_pin:{6}, backL2_pin:{7}".format(
                frontR1_pin, frontR2_pin, frontL1_pin, frontL2_pin, backR1_pin, backR2_pin, backL1_pin, backL2_pin))
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.frontR1_pin, GPIO.OUT)
        GPIO.setup(self.frontR2_pin, GPIO.OUT)
        GPIO.setup(self.frontL1_pin, GPIO.OUT)
        GPIO.setup(self.frontL2_pin, GPIO.OUT)
        GPIO.setup(self.backR1_pin, GPIO.OUT)
        GPIO.setup(self.backR2_pin, GPIO.OUT)
        GPIO.setup(self.backL1_pin, GPIO.OUT)
        GPIO.setup(self.backL2_pin, GPIO.OUT)

        self.motorFR1 = GPIO.PWM(self.frontR1_pin, 500)
        self.motorFR2 = GPIO.PWM(self.frontR2_pin, 500)
        self.motorFL1 = GPIO.PWM(self.frontL1_pin, 500)
        self.motorFL2 = GPIO.PWM(self.frontL2_pin, 500)
        self.motorBR1 = GPIO.PWM(self.backR1_pin, 500)
        self.motorBR2 = GPIO.PWM(self.backR2_pin, 500)
        self.motorBL1 = GPIO.PWM(self.backL1_pin, 500)
        self.motorBL2 = GPIO.PWM(self.backL2_pin, 500)

        self.motorFR1.start(0)
        self.motorFR2.start(0)
        self.motorFL1.start(0)
        self.motorFL2.start(0)
        self.motorBR1.start(0)
        self.motorBR2.start(0)
        self.motorBL1.start(0)
        self.motorBL2.start(0)

    def stop(self):
        self.motorFR1.ChangeDutyCycle(0)
        self.motorFR2.ChangeDutyCycle(0)
        self.motorFL1.ChangeDutyCycle(0)
        self.motorFL2.ChangeDutyCycle(0)
        self.motorBR1.ChangeDutyCycle(0)
        self.motorBR2.ChangeDutyCycle(0)
        self.motorBL1.ChangeDutyCycle(0)
        self.motorBL2.ChangeDutyCycle(0)

    def forward(self, speed):
        self.motorFR1.ChangeDutyCycle(speed)
        self.motorFR2.ChangeDutyCycle(0)
        self.motorFL1.ChangeDutyCycle(speed)
        self.motorFL2.ChangeDutyCycle(0)
        self.motorBR1.ChangeDutyCycle(speed)
        self.motorBR2.ChangeDutyCycle(0)
        self.motorBL1.ChangeDutyCycle(speed)
        self.motorBL2.ChangeDutyCycle(0)

    def backword(self, speed):
        self.motorFR1.ChangeDutyCycle(0)
        self.motorFR2.ChangeDutyCycle(speed)
        self.motorFL1.ChangeDutyCycle(0)
        self.motorFL2.ChangeDutyCycle(speed)
        self.motorBR1.ChangeDutyCycle(0)
        self.motorBR2.ChangeDutyCycle(speed)
        self.motorBL1.ChangeDutyCycle(0)
        self.motorBL2.ChangeDutyCycle(speed)

    def rotateClockwise(self, speed):
        self.motorFR1.ChangeDutyCycle(0)
        self.motorFR2.ChangeDutyCycle(speed)
        self.motorFL1.ChangeDutyCycle(speed)
        self.motorFL2.ChangeDutyCycle(0)
        self.motorBR1.ChangeDutyCycle(0)
        self.motorBR2.ChangeDutyCycle(speed)
        self.motorBL1.ChangeDutyCycle(speed)
        self.motorBL2.ChangeDutyCycle(0)

    def rotateCounterclockwise(self, speed):
        self.motorFR1.ChangeDutyCycle(speed)
        self.motorFR2.ChangeDutyCycle(0)
        self.motorFL1.ChangeDutyCycle(0)
        self.motorFL2.ChangeDutyCycle(speed)
        self.motorBR1.ChangeDutyCycle(speed)
        self.motorBR2.ChangeDutyCycle(0)
        self.motorBL1.ChangeDutyCycle(0)
        self.motorBL2.ChangeDutyCycle(speed)

    def setSpeed(self, lSpeed, rSpeed):
        if lSpeed < 0:
            self.motorFL1.ChangeDutyCycle(0)
            self.motorFL2.ChangeDutyCycle(-lSpeed)
            self.motorBL1.ChangeDutyCycle(0)
            self.motorBL2.ChangeDutyCycle(-lSpeed)
        elif lSpeed >= 0:
            self.motorFL1.ChangeDutyCycle(lSpeed)
            self.motorFL2.ChangeDutyCycle(0)
            self.motorBL1.ChangeDutyCycle(lSpeed)
            self.motorBL2.ChangeDutyCycle(0)

        if rSpeed < 0:
            self.motorFR1.ChangeDutyCycle(0)
            self.motorFR2.ChangeDutyCycle(-rSpeed)
            self.motorBR1.ChangeDutyCycle(0)
            self.motorBR2.ChangeDutyCycle(-rSpeed)
        elif rSpeed >= 0:
            self.motorFR1.ChangeDutyCycle(rSpeed)
            self.motorFR2.ChangeDutyCycle(0)
            self.motorBR1.ChangeDutyCycle(rSpeed)
            self.motorBR2.ChangeDutyCycle(0)

    def __delete__(self):
        self.motorFR1.stop()
        self.motorFR2.stop()
        self.motorFL1.stop()
        self.motorFL2.stop()
        self.motorBR1.stop()
        self.motorBR2.stop()
        self.motorBL1.stop()
        self.motorBL2.stop()
        GPIO.cleanup()
