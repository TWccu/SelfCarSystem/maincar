import json
import socket
import threading
import time

import cv2
import num_detector as num_detector
import numpy as np
import paho.mqtt.publish as publish
from hcsr04 import hcsr04
from motor_controller import CCUPiMotor_4Motor
from picamera import PiCamera
from picamera.array import PiRGBArray

minLineLength = 100
maxLineGap = 20
height = 480
weight = 640
areaHeight = 250
ccuPiMotor = CCUPiMotor_4Motor(38, 40, 35, 36, 32, 33, 29, 31)
camera = PiCamera()
camera.resolution = (weight, height)
camera.framerate = 30
rawCapture = PiRGBArray(camera, size=(weight, height))
time.sleep(0.1)
missionState = 1
gateRate = 0
tempNum = 0
counterCall = -300
maxSpeed = 60
turnSpeed = 100 - maxSpeed
turnState = 0
hasGateCounter = 0
counter = 0

with open('selfCarConfig.json') as f:
    data = json.load(f)
    mqtt_in_topic = data["mqtt_in_topic"]
    print("mqtt_in_topic: ", mqtt_in_topic)
    mqtt_out_topic = data["mqtt_out_topic"]
    print("mqtt_out_topic: ", mqtt_out_topic)
    # mqtt_broker = data["mqtt_broker"]
    # print("mqtt_broker: ", mqtt_broker)
    # mqtt_port = data["mqtt_port"]
    # print("mqtt_port: ", mqtt_port)

client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
client.bind(("", 23130))
data, addr = client.recvfrom(1024)
payload = json.loads(data.decode('utf-8'))
mqtt_broker = payload['mqttip']
mqtt_port = payload['mqttport']
print('MQTT Server ' + mqtt_broker + ':' + str(mqtt_port))


'''確定數字用，重複辨識出一樣的數字才做gateRate的累加'''
def checkNumRate(digit):
    global tempNum
    global gateRate
    if digit == -1:
        return
    if tempNum == digit:
        gateRate += 1
    else:
        tempNum = digit
        gateRate = 0


'''判斷是否有閘門'''
def hasGate(distance):
    global hasGateCounter
    hc04 = hcsr04(22, 24)
    step = True
    while True:
        _distance = hc04.distance()
        if _distance <= distance:
            hasGateCounter = 3
        else:
            hasGateCounter -= 1

if __name__ == '__main__':
    
    '''建立超音波偵測的執行序，並執行'''    
    gate_detector = threading.Thread(target=hasGate, args=(50,))  ##異步
    gate_detector.start()
    
    for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
        '''高斯模糊'''
        camera = cv2.GaussianBlur(frame.array[:, :], (5, 5), 0)
        '''看是否有閘門'''
#        print(hasGateCounter)
        if hasGateCounter > 0:
            '''停止馬達'''
            ccuPiMotor.stop()
            '''只擷取路線範圍的影像'''
            gateImage = frame.array[:230, 100:]
            ##    cv2.imshow("gateImage",gateImage)
            '''擷取門上數字'''
            gateNum = num_detector.findNum(gateImage, 10000, 40000)
            '''確認數字是否重複、穩定'''
            checkNumRate(gateNum)
            # if (missionState == 1 and gateNum != -1 and gateRate == 10 and counterCall <= -300):
            if (missionState == 1 and gateNum != -1 and gateRate == 10):
                '''missionState 的轉換可避免一直publish請求開門的訊息'''
                missionState = 0
                message = "door," + str(gateNum)
                '''publish請求開門訊息'''
                publish.single("toCar2", message, qos=1, hostname=mqtt_broker)
            '''清除影像快取'''
            rawCapture.truncate(0)
            continue

        '''轉灰階'''
        camera = cv2.cvtColor(camera, cv2.COLOR_BGR2GRAY)
        '''只擷取路線範圍的影像'''
        roadImage = camera[350:, :]
        ##    cv2.imshow("roadImage",roadImage)
        '''轉黑白'''
        roadImage = cv2.threshold(roadImage, 150, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

        right = np.sum(roadImage[:, 440:], dtype=int)
        left = np.sum(roadImage[:, :200], dtype=int)
        
#        print(right - left)
#        cv2.imshow('original', roadImage)

        if (hasGateCounter <= 0):
            missionState = 1
            if (right - left) > 8000:
                if (turnState != 1):
                    turnState = 1
                    counter = 0
#                ccuPiMotor.setSpeed(maxSpeed + counter, -20 - counter)
                ccuPiMotor.setSpeed(70, -40)
                counter += 0.1
                if counter > turnSpeed:
                    counter = turnSpeed
            elif (right - left) < -8000:
                if (turnState != -1):
                    turnState = -1
                    counter = 0
#                ccuPiMotor.setSpeed(-20 - counter, maxSpeed + counter)
                ccuPiMotor.setSpeed(-40, 70)
                counter += 0.1
                if counter > turnSpeed:
                    counter = turnSpeed
            else:
                counter = 0
                ccuPiMotor.setSpeed(50, 50)

        key = cv2.waitKey(1) & 0xFF
        rawCapture.truncate(0)

        if key == ord("q"):
            break
