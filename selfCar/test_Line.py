from motor_controller import CCUPiMotor
from pynput import keyboard

ccuPiMotor = CCUPiMotor(13, 11, 18, 16)
speed = 40

def on_press(key):
    try:
        a = format(key.char)
        if a == 'w':
            ccuPiMotor.forward(speed)
        elif a == 'a':
            ccuPiMotor.rotateCounterclockwise(speed)
        elif a == 's':
            ccuPiMotor.backword(speed)
        elif a == 'd':
            ccuPiMotor.rotateClockwise(speed)
    except AttributeError:
        print('special key {0} pressed'.format(
            key))


def on_release(key):
    ccuPiMotor.stop()
    if key == keyboard.Key.esc:
        # Stop listener
        return False


with keyboard.Listener(
        on_press=on_press,
        on_release=on_release) as listener:
    listener.join()
