from find_line import SelfCarEyes
from motor_controller import CCUPiMotor
import paho.mqtt.client as mqtt
import threading
import json

from car_state import CarState

carState = CarState()
mc = CCUPiMotor(13, 11, 18, 16)
##autoController = SelfCarEyes(carState, mc)

with open('selfCarConfig.json') as f:
    data = json.load(f)
    mqtt_in_topic = data["mqtt_in_topic"]
    print("mqtt_in_topic: ", mqtt_in_topic)
    mqtt_out_topic = data["mqtt_out_topic"]
    print("mqtt_out_topic: ", mqtt_out_topic)
    mqtt_broker = data["mqtt_broker"]
    print("mqtt_broker: ", mqtt_broker)
    mqtt_port = data["mqtt_port"]
    print("mqtt_port: ", mqtt_port)

def on_connect(client, userData, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe(mqtt_in_topic)


def spilt_message(msgs):
    return msgs.split(',')


def state_center(args):
    args = int(args)
    if args == -1:
        msg = 'state,'+ str(carState.get_state())
        response_mqtt(msg)
    elif args == 1:
        carState.set_state(1)
##        follow_thread = threading.Thread(autoController.follow_line)##異步
##        follow_thread.start()
        response_mqtt('OK')
    elif args == 0:
        carState.set_state(0)
        response_mqtt('OK')



def remote_center(args):
    if (carState.get_state() != 0):
        response_mqtt('error,please set state to 0')
        return
    mc.setSpeed(float(args[1]), float(args[3]))


def response_mqtt(msgs):
    client.publish(mqtt_out_topic, msgs)


def on_message(client, userData, msg):
    msgs = msg.payload.decode("utf8")
    msgs_array = spilt_message(msgs)
    if msgs_array[0] == 'state':
        state_center(msgs_array[1])
    elif msgs_array[0] == 'm':
        remote_center(msgs_array[1:5])
    elif msgs_array[0] =='Connect':
        response_mqtt(carState.get_state())


if __name__ == '__main__':
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(mqtt_broker, mqtt_port, 60)
    client.loop_forever()
