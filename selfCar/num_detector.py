import cv2
import imutils
import numpy as np

DIGITS_LOOKUP = {
    (1, 1, 1, 0, 1, 1, 1): 0,
    (0, 0, 1, 0, 0, 1, 0): 1,
    (1, 0, 1, 1, 1, 0, 1): 2,
    (1, 0, 1, 1, 0, 1, 1): 3,
    (0, 1, 1, 1, 0, 1, 0): 4,
    (1, 1, 0, 1, 0, 1, 1): 5,
    (1, 1, 0, 1, 1, 1, 1): 6,
    (1, 0, 1, 0, 0, 1, 0): 7,
    (1, 1, 1, 1, 1, 1, 1): 8,
    (1, 1, 1, 1, 0, 1, 1): 9
}


def filterRed(image):
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    lower_red = np.array([156, 85, 150])
    upper_red = np.array([180, 255, 255])

    mask_red = cv2.inRange(hsv, lower_red, upper_red)
    res_red = cv2.bitwise_and(image, image, mask=mask_red)
    res_red = cv2.cvtColor(res_red, cv2.COLOR_BGR2GRAY)
    res_red = cv2.threshold(res_red, 30, 255, cv2.THRESH_BINARY)[1]
    return res_red


def getNum(warped):
    try:

        warpedBinaryImage = filterRed(warped)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        warpedBinaryImage = cv2.dilate(warpedBinaryImage, kernel, iterations=2)
        cv2.imshow("textBox", warpedBinaryImage)
        areaBios = 8
        (x, y, numberAreaWight, numberAreaHigh) = cv2.boundingRect(warpedBinaryImage)

        halfNumberAreaHigh = numberAreaHigh * 0.7
        oneThirdNumberAreaHigh = numberAreaHigh * 0.3
        image, numberConts, numberHierarchy = cv2.findContours(warpedBinaryImage, cv2.RETR_TREE,
                                                               cv2.CHAIN_APPROX_SIMPLE)
        maxNumberW = warped.shape[1] * 0.4
        usedNumberConts = []
        for i in range(len(numberConts)):
            (ix, iy, iw, ih) = cv2.boundingRect(numberConts[i])

            if ih > oneThirdNumberAreaHigh and numberHierarchy[0][i][3] == -1:
                (x, y, w, h) = cv2.boundingRect(numberConts[i])
                usedNumberConts.append(numberConts[i])
        thresh3 = cv2.cvtColor(warpedBinaryImage, cv2.COLOR_GRAY2BGR)
        for c in usedNumberConts:
            # extract the digit ROI
            (x, y, w, h) = cv2.boundingRect(c)

            roi = warpedBinaryImage[y:y + h, x:x + w]
            testImg = cv2.cvtColor(roi, cv2.COLOR_GRAY2BGR)
            # compute the width and height of each of the 7 segments
            # we are going to examine
            (roiH, roiW) = roi.shape
            (dW, dH) = (int(roiW * 0.2), int(roiH * 0.125))
            dHC = int(dH * 0.5)

            segments = [
                ((2 * dW - dW // 2, 0), (3 * dW + dW, dH)),  # top
                ((dW // 4, dH), (2 * dW - dW // 2, 3 * dH + dHC)),  # top-left
                ((4 * dW + dW // 3, dH), (w, 3 * dH)),  # top-right
                ((2 * dW - dW // 2, 4 * dH - dHC // 2), (3 * dW + dW // 2, 4 * dH + dHC * 3)),  # center
                ((dW // 3, 5 * dH), (dW + dW // 3, 8 * dH)),  # bottom-left
                ((4 * dW - dW // 2, 5 * dH), (w - (dW // 2), 7 * dH)),  # bottom-right
                ((2 * dW - dW // 2, h - 3 * dHC), (3 * dW + dW // 2, h))  # bottom
            ]

            on = [0] * len(segments)
            # loop over the segments
            for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
                segROI = roi[yA:yB, xA:xB]
                total = cv2.countNonZero(segROI)
                area = (xB - xA) * (yB - yA)
                if total / float(area) > 0.5:
                    on[i] = 1

                # cv2.rectangle(testImg, (xA, yA), (xB, yB), (0, 255, 0), 1)
                # print(i)
            # lookup the digit and draw it on the image

            # cv2.imshow("testIMg", testImg)
            digit = DIGITS_LOOKUP[tuple(on)]
            # cv2.rectangle(thresh3, (x, y), (x + w, y + h), (0, 255, 0), 1)
            # cv2.putText(thresh3, str(digit), (x + 15, y + 15), cv2.FONT_HERSHEY_SIMPLEX, 0.3, (255, 0, 0), 1)
            # cv2.imshow("warped", thresh3)
            print('Num: ' + str(digit))
            return digit
    except:
        ##        print('can know number...')
        return -1


def findNum(image, minArea, maxArea):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    binaryImage = cv2.threshold(gray, 80, 255, cv2.THRESH_BINARY_INV)[1]
    edged = cv2.Canny(binaryImage, 30, 150)
    cntsLevel = cv2.findContours(edged, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    cntsLevel = cntsLevel[0] if imutils.is_cv2() else cntsLevel[1]
    rect = None

    for c in cntsLevel:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.03 * peri, True)
        if len(approx) == 4:
            area = cv2.contourArea(approx)
            if ((area > minArea) and (area < maxArea)):
                rect = approx
                break

    if rect is not None:
        tempRect = rect.reshape(4, 2)
        maxX = max(tempRect[:, 0])
        minX = min(tempRect[:, 0])
        maxY = max(tempRect[:, 1]) + 10
        minY = min(tempRect[:, 1])
        warped = image[minY:maxY, minX:maxX]
        num = getNum(warped)
        return num
    return -1
